import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../productDetails.css";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import _ from "lodash";
import { toast } from "react-toastify";

const ProductPage = () => {
  const navigate = useNavigate();
  const [product, setProduct]: any = useState({});
  let params = new URLSearchParams(window.location.search);
  const token = localStorage.getItem("token");
  const product_id = params.get("id");

  const fetchProduct = async () => {
    try {
      await fetch(`https://dummyjson.com/products/${product_id}`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((json) => setProduct(json));
    } catch (error: any) {
      toast.error("Unknown Error", {
        position: "top-right",
      });
    }
  };

  useEffect(() => {
    fetchProduct();
  }, [product_id]);

  let cartOrders = JSON.parse(
    JSON.parse(JSON.stringify(localStorage.getItem("cart_items")) || "[]")
  );

  return (
    <>
      <Grid container>
        <Card className="">
          <CardMedia
            image={product.images && product.images[0]}
            title={product.title}
            component="img"
          />
          <CardContent className="">
            <Typography variant="h5" component="h2" gutterBottom>
              Name: {product.title}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary" gutterBottom>
              Category: {product.category}
            </Typography>
            <Typography variant="body1" gutterBottom>
              {product.description}
            </Typography>
            <Typography variant="h6" color="textSecondary">
              Price: ${product.price}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              variant="outlined"
              color="primary"
              size="small"
              onClick={() => {
                let cart_items = JSON.parse(
                  localStorage.getItem("cart_items") || "[]"
                );
                let new_items_added = [...cart_items, product];
                localStorage.setItem(
                  "cart_items",
                  JSON.stringify(new_items_added)
                );
                navigate(`/product-details?id=${product.id}`);
              }}
              disabled={Boolean(_.find(cartOrders, { id: product.id }))}
            >
              Add to Cart
            </Button>
          </CardActions>
        </Card>
      </Grid>
    </>
  );
};

export default ProductPage;
