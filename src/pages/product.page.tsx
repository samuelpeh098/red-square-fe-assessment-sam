import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../productList.css";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import React from "react";
import _ from "lodash";
import Pagination from "@mui/material/Pagination";
import { toast } from "react-toastify";

interface Product {
  title: string;
  year: number;
}

interface Category {
  title: string;
}

function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

const ProductPage = () => {
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);
  const [catProducts, setCatProducts] = useState([]);
  const fetchUser = async () => {
    try {
      await fetch("https://dummyjson.com/products", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((json) => setProducts(json.products));

      await fetch("https://dummyjson.com/products/categories", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then((json) => setCatProducts(json));
    } catch (error: any) {
      toast.error("Unknown Error", {
        position: "top-right",
      });
    }
  };

  useEffect(() => {
    fetchUser();
  }, []);

  const token = localStorage.getItem("token");
  const [open, setOpen] = React.useState(false);
  const [openCat, setOpenCat] = React.useState(false);
  const [options, setOptions] = React.useState<readonly Product[]>([]);
  const loading = open && options.length === 0;
  const [catgegoryOptions, setCategoryOptions] = React.useState<
    readonly Category[]
  >([]);
  const loading2 = openCat && catgegoryOptions.length === 0;

  React.useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }

    (async () => {
      await sleep(1e3);

      if (active) {
        setOptions([...products]);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading]);

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  React.useEffect(() => {
    let active = true;

    if (!loading2) {
      return undefined;
    }

    (async () => {
      await sleep(1e3);

      if (active) {
        setCategoryOptions([...catProducts]);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading2]);

  React.useEffect(() => {
    if (!openCat) {
      setCategoryOptions([]);
    }
  }, [openCat]);

  let cartOrders = JSON.parse(
    JSON.parse(JSON.stringify(localStorage.getItem("cart_items")) || "[]")
  );

  const [currentPage, setCurrentPage] = useState(1);
  const productsPerPage = 10;
  const totalProducts = products && products.length;
  const totalPages = Math.ceil(totalProducts / productsPerPage);
  const startIndex = (currentPage - 1) * productsPerPage;
  const endIndex = startIndex + productsPerPage;
  const currentProducts = products.slice(startIndex, endIndex);

  const handlePageChange = (event: any, page: any) => {
    setCurrentPage(page);
  };

  const [showPriceDesc, setShowPriceDesc] = useState(false);
  const toggleSortPriceDesc = () => {
    setShowPriceDesc(!showPriceDesc);
  };

  return (
    <>
      {token ? (
        <Grid container>
          <Grid item xs={12} sm={12} md={2}>
            <Pagination
              count={totalPages}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
            />
          </Grid>
          <Grid item xs={12} sm={12} md={2}>
            <Autocomplete
              id="search-product"
              open={open}
              onOpen={() => {
                setOpen(true);
              }}
              onClose={() => {
                setOpen(false);
              }}
              isOptionEqualToValue={(option, value) =>
                option.title === value.title
              }
              getOptionLabel={(option) => option.title}
              options={options}
              loading={loading}
              onChange={(e: any, v: any) => {
                navigate(`/product-details?id=${v.id}`);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Search Product"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        <a href="/">{params.InputProps.endAdornment}</a>
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={2}>
            <Autocomplete
              id="search-category"
              open={openCat}
              onOpen={() => {
                setOpenCat(true);
              }}
              onClose={() => {
                setOpenCat(false);
              }}
              isOptionEqualToValue={(option, value) =>
                option.title === value.title
              }
              onChange={(e: any, v: any) => {
                navigate(`/category?name=${v}`);
              }}
              getOptionLabel={(option) => option}
              options={catgegoryOptions}
              loading={loading2}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Search Category"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading2 ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
          </Grid>
          <Button
            size="medium"
            variant={showPriceDesc ? "contained" : "outlined"}
            color={showPriceDesc ? "success" : "warning"}
            onClick={() => {
              toggleSortPriceDesc();
            }}
          >
            Sort By Price
          </Button>
          {showPriceDesc
            ? _.sortBy(currentProducts, ["price"]).map((product: any) => (
                <Grid item xs={12} sm={12} md={2} key={product.id}>
                  <Card sx={{ maxWidth: 345 }}>
                    <CardMedia
                      sx={{ height: 140 }}
                      image={product.thumbnail}
                      title={product.title}
                      component="img"
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        {product.title} (
                        {product.category.charAt(0).toUpperCase() +
                          product.category.slice(1)}
                        )
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {product.description}
                      </Typography>
                      <Typography gutterBottom variant="h7" component="div">
                        RM{product.price.toFixed(2)}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        variant="outlined"
                        color="primary"
                        size="small"
                        onClick={() => {
                          let cart_items = JSON.parse(
                            localStorage.getItem("cart_items") || "[]"
                          );
                          let new_items_added = [...cart_items, product];
                          localStorage.setItem(
                            "cart_items",
                            JSON.stringify(new_items_added)
                          );
                          navigate(`/product`);
                        }}
                        disabled={Boolean(
                          _.find(cartOrders, { id: product.id })
                        )}
                      >
                        Add to Cart
                      </Button>
                      <a href={`/product-details?id=${product.id}`}>
                        <Button size="small" variant="outlined" color="success">
                          View Product
                        </Button>
                      </a>
                    </CardActions>
                  </Card>
                </Grid>
              ))
            : currentProducts.map((product: any) => (
                <Grid item xs={12} sm={12} md={2} key={product.id}>
                  <Card sx={{ maxWidth: 345 }}>
                    <CardMedia
                      sx={{ height: 140 }}
                      image={product.thumbnail}
                      title={product.title}
                      component="img"
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        {product.title} (
                        {product.category.charAt(0).toUpperCase() +
                          product.category.slice(1)}
                        )
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {product.description}
                      </Typography>
                      <Typography gutterBottom variant="h7" component="div">
                        RM{product.price.toFixed(2)}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        variant="outlined"
                        color="primary"
                        size="small"
                        onClick={() => {
                          let cart_items = JSON.parse(
                            localStorage.getItem("cart_items") || "[]"
                          );
                          let new_items_added = [...cart_items, product];
                          localStorage.setItem(
                            "cart_items",
                            JSON.stringify(new_items_added)
                          );
                          navigate(`/product`);
                        }}
                        disabled={Boolean(
                          _.find(cartOrders, { id: product.id })
                        )}
                      >
                        Add to Cart
                      </Button>
                      <a href={`/product-details?id=${product.id}`}>
                        <Button size="small" variant="outlined" color="success">
                          View Product
                        </Button>
                      </a>
                    </CardActions>
                  </Card>
                </Grid>
              ))}
          <Grid item xs={12} sm={12} md={2}>
            <Pagination
              count={totalPages}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
            />
          </Grid>
        </Grid>
      ) : (
        "Not Authorized to view. Please login to view this page."
      )}
    </>
    // <div className="product-list">

    //   <div className="product-grid">

    //   </div>
    // </div>
  );
};

export default ProductPage;
