export interface IUser {
  id: string;
  username: string;
  email: string;
}

export interface IProduct {
  id: number;
  title: string;
}
