import {combineReducers} from 'redux'
import { cartOrders } from './reducer'
// import { historyVipOrders } from './reducer'
// import { historyPendingOrders } from './reducer'
// import { historyCompletedOrders } from './reducer'
// import { historyBots } from './reducer'

export default combineReducers({
    cartOrders,
    // historyVipOrders,
    // historyPendingOrders,
    // historyCompletedOrders,
    // historyBots,
})