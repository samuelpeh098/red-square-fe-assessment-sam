import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import { Box } from "@mui/material";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Badge from "@mui/material/Badge";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";

const Header = () => {
  const navigate = useNavigate();
  const token = localStorage.getItem("token");
  const [showCartModal, setShowCartModal] = useState(false);
  const toggleCart = () => {
    setShowCartModal(!showCartModal);
  };
  let cartOrders = JSON.parse(
    JSON.parse(JSON.stringify(localStorage.getItem("cart_items")) || "[]")
  );

  return (
    <>
      <Grid container item xs={12}>
        <header className="bg-white h-20">
          <nav className="h-full flex justify-between container items-center">
            <div>
              <Link to="/" className="text-ct-dark-600 text-2xl font-semibold">
                Red Square
              </Link>
            </div>
          </nav>
        </header>
        <ul className="flex items-center gap-4">
          {!token && (
            <>
              <li>
                <Link to="/login" className="text-ct-dark-600">
                  Login
                </Link>
              </li>
            </>
          )}
          {token && (
            <>
              <li>
                <Link to="/product" className="text-ct-dark-600">
                  Product
                </Link>
              </li>
              <li>
                <Badge
                  badgeContent={cartOrders && cartOrders.length}
                  color="primary"
                >
                  <ShoppingCartIcon
                    color="action"
                    onClick={() => {
                      toggleCart();
                    }}
                    fontSize="small"
                  />
                </Badge>
              </li>
              <li>
                <Button
                  variant="outlined"
                  color="primary"
                  size="small"
                  onClick={() => {
                    localStorage.removeItem("token");
                    navigate("/login");
                  }}
                >
                  Logout
                </Button>
              </li>
            </>
          )}
        </ul>
      </Grid>
      <Grid item xs={6}>
        {showCartModal ? (
          <Box sx={{ bgcolor: "background.paper" }}>
            <List>
              {cartOrders && cartOrders.length > 0
                ? cartOrders.map((e: any) => {
                    return (
                      <>
                        <Link to={`/product-details?id=${e.id}`}>
                          <ListItem disablePadding>
                            <ListItemButton>
                              <ListItemText primary={e.title} />
                            </ListItemButton>
                          </ListItem>
                          <Divider />
                        </Link>
                      </>
                    );
                  })
                : "Cart is empty"}
            </List>
          </Box>
        ) : (
          ""
        )}
      </Grid>
    </>
  );
};

export default Header;
