import type { RouteObject } from "react-router-dom";
import Layout from "../components/Layout";
import HomePage from "../pages/home.page";
import LoginPage from "../pages/login.page";
import ProductPage from "../pages/product.page";
import ProductDetailsPage from "../pages/product-details.page";
import CategoryPage from "../pages/category.page";

const normalRoutes: RouteObject = {
  path: "*",
  element: <Layout />,
  children: [
    {
      index: true,
      element: <HomePage />,
    },
    {
      path: "product",
      element: <ProductPage />,
    },
    {
      path: "login",
      element: <LoginPage />,
    },
    {
      path: "product-details",
      element: <ProductDetailsPage />,
    },
    {
      path: "category",
      element: <CategoryPage />,
    },
  ],
};

const routes: RouteObject[] = [normalRoutes];

export default routes;
